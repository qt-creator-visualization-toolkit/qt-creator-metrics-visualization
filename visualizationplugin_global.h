#ifndef VISUALIZATIONPLUGIN_GLOBAL_H
#define VISUALIZATIONPLUGIN_GLOBAL_H

#include <QtCore/QtGlobal>

#if defined(VISUALIZATIONPLUGIN_LIBRARY)
#  define VISUALIZATIONPLUGINSHARED_EXPORT Q_DECL_EXPORT
#else
#  define VISUALIZATIONPLUGINSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // VISUALIZATIONPLUGIN_GLOBAL_H

