#ifndef VISUALIZATIONOUTPUTPANE_H
#define VISUALIZATIONOUTPUTPANE_H

#include <coreplugin/ioutputpane.h>

class QGraphicsView;
class QGraphicsScene;
class QDeclarativeView;

class MetricCollector;

class VisualizationOutputPane : public Core::IOutputPane
{
    Q_OBJECT

public:
    VisualizationOutputPane(MetricCollector *metricCollector, QObject *parent);
    ~VisualizationOutputPane();

    // Virtual pure functions from Core::IOutputPane
    virtual QWidget *outputWidget(QWidget *parent);
    virtual QList<QWidget *> toolBarWidgets() const;
    virtual QString displayName() const;
    virtual int priorityInStatusBar() const;
    virtual void clearContents();
    virtual void visibilityChanged(bool visible);
    virtual void setFocus();
    virtual bool hasFocus() const;
    virtual bool canFocus() const;
    virtual bool canNavigate() const;
    virtual bool canNext() const;
    virtual bool canPrevious() const;
    virtual void goToNext();
    virtual void goToPrev();

public Q_SLOTS:
    void updateScene();

private:
    QGraphicsScene *m_scene;
    QGraphicsView *m_view;

    QDeclarativeView *m_qmlViewer;

    MetricCollector *m_metricCollector;
};

#endif // VISUALIZATIONOUTPUTPANE_H
