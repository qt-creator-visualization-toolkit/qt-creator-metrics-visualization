import QtQuick 1.0

Rectangle {
    id: rectangle1

    Column {
        id: menu
        width: parent.width / 5
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.verticalCenter: parent.verticalCenter
        spacing: 10
        Button {
            id: methods
            text: qsTr("Methods")
            onButtonClicked: {
                        for (var i = 0; i < graph.children.length; ++i)
                            graph.children[i].destroy();
                        var projectClasses = metricsCollector.projectClassesToVariantList();
                        var classesFunctions = metricsCollector.classFunctionsToVariantMap();
                        for (var i = 0; i < projectClasses.length; ++i)
                        {
                            var rect = Qt.createQmlObject("import QtQuick 1.0; Rectangle { color: 'red'; width: 20; }", graph);
                            rect.x = i*30;
                            rect.height = classesFunctions[projectClasses[i]]*10;
                            rect.y = graph.height / 2 - rect.height
                        }
                       }
        }
        Button {
            id: attributes
            text: qsTr("Attributes")
        }
    }

    Rectangle {
        id: graph
        width: 4*(parent.width / 5)
        height: parent.height
        color: "#ffffff"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.verticalCenter: parent.verticalCenter
        Rectangle {
            anchors.centerIn: parent
            width: 100
            height: 50
            border.color: "#000000"
        }
    }


}
