#include "visualizationoutputpane.h"

#include <QtCore/QHash>
#include <QtCore/QDebug>

#include <QtGui/QGraphicsView>
#include <QtGui/QGraphicsRectItem>
#include <QtGui/QGraphicsLineItem>

#include <QtDeclarative/QDeclarativeView>
#include <QtDeclarative/QDeclarativeContext>

#include <projectexplorer/projectnodes.h>

#include <cplusplus/Symbols.h>
#include <cplusplus/Literals.h>

#include "metriccollector.h"

VisualizationOutputPane::VisualizationOutputPane(MetricCollector *metricCollector, QObject *parent)
    : Core::IOutputPane(parent), m_scene(new QGraphicsScene), m_view(new QGraphicsView), m_qmlViewer(new QDeclarativeView), m_metricCollector(metricCollector)
{
    m_view->setScene(m_scene);
    m_qmlViewer->setSource(QUrl::fromLocalFile("/root/src/qt-creator-metrics-visualization/metrics.qml"));
    m_qmlViewer->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    m_qmlViewer->rootContext()->setContextProperty("metricsCollector", metricCollector);
}

VisualizationOutputPane::~VisualizationOutputPane()
{
    delete m_view;
    delete m_scene;
    delete m_qmlViewer;
}

QWidget *VisualizationOutputPane::outputWidget(QWidget *parent)
{
//    m_view->setParent(parent);
//    return m_view;
    m_qmlViewer->setParent(parent);
    return m_qmlViewer;
}

QList<QWidget *> VisualizationOutputPane::toolBarWidgets() const
{
    return QList<QWidget *>();
}

QString VisualizationOutputPane::displayName() const
{
    return tr("Visualization");
}

int VisualizationOutputPane::priorityInStatusBar() const
{
    return 0;
}

void VisualizationOutputPane::clearContents()
{
}

void VisualizationOutputPane::visibilityChanged(bool visible)
{
    Q_UNUSED(visible)
}

void VisualizationOutputPane::setFocus()
{
}

bool VisualizationOutputPane::hasFocus() const
{
    return false;
}

bool VisualizationOutputPane::canFocus() const
{
    return false;
}

bool VisualizationOutputPane::canNavigate() const
{
    return false;
}

bool VisualizationOutputPane::canNext() const
{
    return false;
}

bool VisualizationOutputPane::canPrevious() const
{
    return false;
}

void VisualizationOutputPane::goToNext()
{
}

void VisualizationOutputPane::goToPrev()
{
}

void VisualizationOutputPane::updateScene()
{
    foreach(ProjectExplorer::ProjectNode *project, m_metricCollector->m_projectClasses.uniqueKeys())
    {
        if (!project) continue;
        qDebug() << "Project: " << project->displayName();
        foreach(CPlusPlus::Class *clazz, m_metricCollector->m_projectClasses.values(project))
        {
            if (clazz && clazz->name() && clazz->name()->identifier())
            {
                qDebug() << "\tClass: " << clazz->name()->identifier()->chars();
                foreach(QString function, m_metricCollector->m_projectClassFunctions.values(MetricCollector::ProjectClass(project, clazz)))
                    qDebug() << "\t\tMethod: " << function << " with " << m_metricCollector->m_projectClassFunctionInfo.value(function);
            }
        }
    }

//    m_scene->clear();
//    m_scene->addLine(0, 0, 400, 0);
//    m_scene->addLine(0, 0, 0, -200);
//    int i = 20;
//    int meanFunctionPerClass = m_metricCollector->m_sumFunctionsAllClasses / m_metricCollector->m_projectClasses.size();
//    foreach(ProjectExplorer::ProjectNode *project, m_metricCollector->m_projectClasses.uniqueKeys())
//    {
//        foreach(CPlusPlus::Class *clazz, m_metricCollector->m_projectClasses.values(project))
//        {
//            if (clazz->name() && clazz->name()->identifier())
//            {
//                int numberOfMethods = m_metricCollector->m_projectClassFunctions.count(MetricCollector::ProjectClass(project, clazz));
//                QGraphicsRectItem *rect = m_scene->addRect(0, -numberOfMethods*10, 20, numberOfMethods*10, QPen(), QBrush(Qt::cyan, Qt::SolidPattern));
//                rect->setPos(i, 0);
//                rect->setToolTip(clazz->name()->identifier()->chars());
//                m_scene->addLine(0, 0, 20, 0)->setPos(i, -meanFunctionPerClass*10);
//                i += 40;
//            }
//        }
//    }
}
