#ifndef VISUALIZATIONPLUGINCONSTANTS_H
#define VISUALIZATIONPLUGINCONSTANTS_H

namespace VisualizationPlugin {
namespace Constants {

const char ACTION_ID[] = "VisualizationPlugin.Action";
const char MENU_ID[] = "VisualizationPlugin.Menu";

} // namespace VisualizationPlugin
} // namespace Constants

#endif // VISUALIZATIONPLUGINCONSTANTS_H

